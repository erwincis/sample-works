<?php

use Illuminate\Database\Seeder;

class CommentsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('comments')->delete();
        
        \DB::table('comments')->insert(array (
            0 => 
            array (
                'id' => 1,
                'created_at' => '2019-05-28 04:25:55',
                'updated_at' => '2019-05-28 04:25:55',
                'email' => 'erwin@gmail.com',
                'comment' => 'asdasdas',
            ),
            1 => 
            array (
                'id' => 2,
                'created_at' => '2019-05-28 04:28:58',
                'updated_at' => '2019-05-28 04:28:58',
                'email' => 'erwin@gmail.com',
                'comment' => 'asdasdas',
            ),
            2 => 
            array (
                'id' => 3,
                'created_at' => '2019-05-28 04:29:14',
                'updated_at' => '2019-05-28 04:29:14',
                'email' => 'erwin@gmail.com',
                'comment' => 'asdasdas',
            ),
            3 => 
            array (
                'id' => 4,
                'created_at' => '2019-05-29 23:25:15',
                'updated_at' => '2019-05-29 23:25:15',
                'email' => 'erwin1@gmail.com',
                'comment' => 'erwinerwin',
            ),
        ));
        
        
    }
}