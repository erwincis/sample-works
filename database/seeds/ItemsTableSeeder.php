<?php

use Illuminate\Database\Seeder;

class ItemsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('items')->delete();
        
        \DB::table('items')->insert(array (
            0 => 
            array (
                'id' => 1,
                'created_at' => '2019-05-27 04:50:48',
                'updated_at' => '2019-05-29 02:04:00',
                'name' => 'first Task123123',
                'description' => 'ham with cheese123123',
                'price' => '15.00',
                'stocks' => 213204,
                'img_url' => 'images/1558932648.jpg',
                'category_id' => 2,
                'deleted_at' => '2019-05-29 02:04:00',
            ),
            1 => 
            array (
                'id' => 2,
                'created_at' => '2019-05-27 04:51:04',
                'updated_at' => '2019-05-29 02:04:02',
                'name' => 'second task',
                'description' => 'ham with cheese',
                'price' => '213.00',
                'stocks' => 12310,
                'img_url' => 'images/1558932664.jpeg',
                'category_id' => 3,
                'deleted_at' => '2019-05-29 02:04:02',
            ),
            2 => 
            array (
                'id' => 3,
                'created_at' => '2019-05-28 07:29:09',
                'updated_at' => '2019-05-29 02:04:05',
                'name' => 'third task',
                'description' => '231321',
                'price' => '45.00',
                'stocks' => 0,
                'img_url' => 'images/1559028549.jpeg',
                'category_id' => 1,
                'deleted_at' => '2019-05-29 02:04:05',
            ),
            3 => 
            array (
                'id' => 4,
                'created_at' => '2019-05-29 02:10:27',
                'updated_at' => '2019-05-30 03:03:37',
                'name' => 'Ford Focus',
                'description' => 'Ford new Focus claims its spot at the top of the family hatchback pile by way of its outstanding handling and superbly pliant, well-resolved ride. Having arguably been in slight decline since the death of the Focus mkI, the best-handling family hatchback in history is undoubtedly back to its very best on driver appeal.',
                'price' => '1088000.00',
                'stocks' => 0,
                'img_url' => 'images/1559095827.jpg',
                'category_id' => 1,
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'created_at' => '2019-05-29 02:15:10',
                'updated_at' => '2019-05-30 02:59:33',
                'name' => 'Volkswagen Golf',
                'description' => 'Still the family hatchback benchmark that most of the class aspires to beat and the biggest-selling car in Europe, and the Volkswagen Golf remains a very classy option. Supremely well-rounded to drive and, although not the cheapest, it does enough to justify its high price tag.',
                'price' => '1569000.00',
                'stocks' => 0,
                'img_url' => 'images/1559096417.jpg',
                'category_id' => 1,
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'created_at' => '2019-05-29 02:24:28',
                'updated_at' => '2019-05-30 02:55:51',
                'name' => 'Mazda 3',
                'description' => 'The fourth-generation Mazda 3 could well be the best looking family hatch currently on sale. And, joy of joys, it retains all the qualities that made its predecessor such an appealing contender in this highly competitive class: strong value for money; spry handling; and an atmospheric petrol engine.',
                'price' => '1110000.00',
                'stocks' => 2,
                'img_url' => 'images/1559096668.jpg',
                'category_id' => 1,
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'created_at' => '2019-05-29 02:26:31',
                'updated_at' => '2019-05-29 03:05:21',
                'name' => 'Honda City',
                'description' => 'Honda has added a petrol-manual ZX variant to the City’s lineup. This, Honda says, is a response to ‘high demand’ for the same. Priced at Rs 12.75 lakh, the ZX petrol manual is about Rs 85,000 more expensive than the VX MT, and about Rs 1 lakh cheaper than the ZX CVT variant. Compared to the VX variant, noteworthy additions include side and curtain airbags, and bigger 16-inch alloy wheels. Honda has also made rear parking sensors standard across the range, and added two new colours - Radiant Red Metallic and Lunar Silver Metallic - to the package.',
                'price' => '843000.00',
                'stocks' => 2,
                'img_url' => 'images/1559099121.jpg',
                'category_id' => 2,
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'created_at' => '2019-05-29 03:02:05',
                'updated_at' => '2019-05-29 03:05:49',
                'name' => 'Honda Civic',
            'description' => 'Honda has launched the Civic in India again at a starting price of Rs 17.7 lakh for the base V variant. The mid-spec VX variant has been priced at Rs 19.2 lakh for the petrol and Rs 20.5 lakh for the diesel, while the top-spec ZX variant costs Rs 21 lakh for the petrol and Rs 22.3 lakh for the diesel (all ex-showroom India). It is also being offered with a 3 year, unlimited km warranty, which can be extended to four or five year. The Civic rivals the Skoda Octavia, Toyota Corolla and the Hyundai Elantra. As of now, Honda has received over 1,100 bookings for the Civic.',
                'price' => '1059000.00',
                'stocks' => 3,
                'img_url' => 'images/1559099149.jpg',
                'category_id' => 2,
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'created_at' => '2019-05-29 03:04:35',
                'updated_at' => '2019-05-29 03:04:35',
                'name' => 'Suzuki Ertiga',
                'description' => 'There is a lot to like about the new Suzuki Ertiga more than its sleek and sophisticated looks. First, its roominess. With a wheelbase of 2,740 mm, seven people can fit inside this MPV and still have 113 liters of space for cargo at the back. Secondly, the Ertiga’s seating versatility. Depending on the variant, the second-row seats have a 60:40 split-folding capability while the third-row seats can 50:50 split-fold for additional space',
                'price' => '665000.00',
                'stocks' => 4,
                'img_url' => 'images/1559099075.jpg',
                'category_id' => 3,
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'created_at' => '2019-05-29 03:07:11',
                'updated_at' => '2019-05-29 03:07:11',
                'name' => 'Toyota Avanza',
                'description' => 'To those looking for a cheaper family-hauler alternative from Toyota, this one is your best bet. Price-wise, the base level trim Avanza is almost P250,000 cheaper than the cheapest Innova available. If you’re particularly worried about its capacity, know that its higher trims can also seat seven like the Innova. Additionally, the Avanza’s second and third-row seats also offer more space for luggage by having a 50:50 split and tumble capability',
                'price' => '883000.00',
                'stocks' => 3,
                'img_url' => 'images/1559099231.jpg',
                'category_id' => 3,
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'created_at' => '2019-05-29 03:09:27',
                'updated_at' => '2019-05-29 03:09:27',
                'name' => 'Toyota Fortuner',
            'description' => 'The Toyota Fortuner is a constant forerunner (pun intended) in the Philippines’ SUV category. Though often viewed as a luxury car for the middle class, it’s a shame that the Fortuner is rarely used for what it’s intended to be—an off-road beast. The 2017 Toyota Fortuner boasts remarkable rough terrain performance strengthened b thousands of kilometers of engineering and testing. High quality materials and finish further sets the Fortuner apart from much of its competition.',
                'price' => '1597000.00',
                'stocks' => 2,
                'img_url' => 'images/1559099367.jpg',
                'category_id' => 4,
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'created_at' => '2019-05-29 03:10:50',
                'updated_at' => '2019-05-29 03:10:50',
                'name' => 'Mitsubishi Montero',
                'description' => 'You certainly can’t put a good vehicle down, and the way the Mitsubishi Montero bounced back from the SUA snafu proves it. The 2017 Mitsubishi Montero is easily one of the best SUV options out there for the money, and the slick updates to its looks make it all the more enticing.',
                'price' => '1719000.00',
                'stocks' => 2,
                'img_url' => 'images/1559099450.jpg',
                'category_id' => 4,
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'created_at' => '2019-05-29 03:12:06',
                'updated_at' => '2019-05-29 03:12:06',
                'name' => 'Ford Everest',
                'description' => 'For the longest time, the Ford Everest has stayed true to its front-wheel-drive beginnings. If this is what kept you from buying this Blue Oval SUV all this years, then you’ll be relieved to find out that the 2017 Ford Everest now also comes in rear-wheel-drive, which makes the car not only lighter, but also cheaper.',
                'price' => '1518000.00',
                'stocks' => 1,
                'img_url' => 'images/1559099526.jpg',
                'category_id' => 4,
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}