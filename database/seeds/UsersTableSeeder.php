<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'admin',
                'email' => 'admin@gmail.com',
                'status' => 'active',
                'role' => 'admin',
                'email_verified_at' => NULL,
                'password' => '$2y$10$v0TBJteC3A.dCc9fdeutxedLfGzUvryL7.arjhEB8oZvAC3hbOcOy',
                'remember_token' => NULL,
                'created_at' => NULL,
                'updated_at' => '2019-05-29 00:32:24',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Erwin',
                'email' => 'erwin@gmail.com',
                'status' => 'active',
                'role' => 'user',
                'email_verified_at' => NULL,
                'password' => '$2y$10$xtbbVfRNluvrj6COO4BXF.XKYy4qWREngc/SbOml4.MYK.FoNQiRe',
                'remember_token' => NULL,
                'created_at' => '2019-05-27 06:23:33',
                'updated_at' => '2019-05-29 23:27:44',
            ),
            2 => 
            array (
                'id' => 4,
                'name' => 'Erwin',
                'email' => 'admin1@gmail.com',
                'status' => 'active',
                'role' => 'admin',
                'email_verified_at' => NULL,
                'password' => '$2y$10$Uvx5nDKmp0Yv6ilxidQSfuprNV9DE35KvTKRDBNgZwqaPLqw2J3Qi',
                'remember_token' => NULL,
                'created_at' => '2019-05-29 04:17:51',
                'updated_at' => '2019-05-29 04:17:51',
            ),
            3 => 
            array (
                'id' => 5,
                'name' => 'erwin',
                'email' => 'erwin1@gmail.com',
                'status' => 'deactive',
                'role' => 'user',
                'email_verified_at' => NULL,
                'password' => '$2y$10$JUn1aRP/MrpwCRKI/OZtbu53zPlXk4O9woVo9nXwKZz32e0syae4u',
                'remember_token' => NULL,
                'created_at' => '2019-05-29 04:20:22',
                'updated_at' => '2019-05-29 04:20:22',
            ),
            4 => 
            array (
                'id' => 6,
                'name' => 'User',
                'email' => 'user@gmail.com',
                'status' => 'active',
                'role' => 'user',
                'email_verified_at' => NULL,
                'password' => '$2y$10$.aPKsxHAdhsXnZ.9OTUYReBazHTeDOipeBefafyOYtXiBvnVd6Xjq',
                'remember_token' => NULL,
                'created_at' => '2019-05-30 01:58:26',
                'updated_at' => '2019-05-30 02:46:11',
            ),
        ));
        
        
    }
}