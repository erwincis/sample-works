<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use Hash;
use Illuminate\Support\Facades\Redirect;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function editProfile() {
        $user = Auth::user();

        return view('editProfile', compact('user'));
    }

    public function saveProfile($id, Request $request) {
        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->save();

        return redirect('/home');
    }

    public function showUsers() {
        $users = User::all();
        if (Auth::user()->role == 'user') {
            return redirect('/home');
        }
        return view('users', compact('users'));
    }

    public function editUser($id, Request $request) {
       // dd($request);
       $user = User::find($id);
       $user->status = $request->status;
       $user->save();

       return redirect::back();
    }

    public function deleteUser($id) {
        $user = User::find($id);
        $user->delete();
        return redirect::back();
    }
}
