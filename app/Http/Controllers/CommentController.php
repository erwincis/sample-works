<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App\Comment;
use Redirect;
use Session;

class CommentController extends Controller
{
    public function leaveMessage(Request $request) {
    	$comments = new Comment;
    	$comments->comment = $request->message;
        if (!Auth::user()) {
            $comments->email = $request->email;
            $comments->save();
        } else {
    	   $comments->email = Auth::user()->email;
           $comments->save();
        }
    	Session::flash('success_message', 'Successfully sent the message');
    	return redirect::back();
    }

    public function showMessage() {
    	$comments = Comment::all();

    	return view('messages', compact('comments'));
    }
}
