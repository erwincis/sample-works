<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Item;
use Illuminate\Support\Facades\Redirect;
use Session;
use Auth;
use App\Order;
use App\Status;

class ItemController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function showCars() {
        $items = Item::all();
        // $items = Item::withTrashed()->get(); //admin
        // $items = Item::onlyTrashed()->get(); // only deleted
        if (Auth::user()->status == 'deactive') {
            Session::flash('success_message', 'You are not activate yet');
            return redirect('/home');
        }
        return view('cars.showCars', compact('items'));
    }

    public function addNew() {
    	$categories = Category::all();

        if (Auth::user()->role == 'user' || Auth::user()->status == 'deactive' ) {
        return redirect('/home');
        }
        return view('cars.addNewCar', compact('categories'));
    	
    }

    public function save(Request $request) {
    	$item = new Item;
    	$item->name = $request->name;
    	$item->description = $request->description;
    	$item->price = $request->price;
    	$item->category_id = $request->category;
        $item->stocks = $request->stocks;
    	if($request->hasFile('image')) {
            $image = $request->file('image');
            $image_name = time().".".$image->getClientOriginalExtension();
            $destination = "images/";
            $image->move($destination, $image_name);
            $item->img_url = $destination.$image_name;
        }
    	$item->save();
    	return redirect('/cars/showCars');
    }

    public function editCar($id) {
        $car = Item::find($id);

        return view('cars.editForm', compact('car'));
    }

    public function editSave($id, Request $request) {
        $item = Item::find($id);
        $item->name = $request->name;
        $item->description = $request->description;
        $item->price = $request->price;
        $item->stocks = $request->stocks;
        $item->category_id = $request->category;
        if($request->hasFile('image')) {
            $image = $request->file('image');
            $image_name = time().".".$image->getClientOriginalExtension();
            $destination = "images/";
            $image->move($destination, $image_name);
            $item->img_url = $destination.$image_name;
        }
        $item->save();
        Session::flash('success_message', 'Successfully Edited');
        return redirect('/cars/showCars');
    }

    public function deleteCar($id) {
        $item = Item::find($id);
        
        if(Session::has('garage')) {
        Session::forget("garage.$id");
        }
        $item->delete();

        Session::flash('success_message', 'Successfully Deleted');
        return redirect::back();
    }

    public function buyCar($id, Request $request) {
        if($request->quantity <= 0) {
            Session::flash('success_message', 'No stocks');
            return redirect('/cars/showCars');
        }

        if(Session::has('garage')) {
            $account = Session::get('garage');
        } else{
            $account = [];
        }

        if(isset($account[$id])) {
            $account[$id] += $request->quantity;
        } else {
            $account[$id] = $request->quantity;
        }
        Session::put('garage', $account);
        Session::flash('success_message', 'Successfully Added to your garage');
        return redirect('/cars/showCars');
    }

    public function garage() {
        if (Auth::user()->status == 'deactive') {
            Session::flash('success_message', 'You are not activate yet');
            return redirect('/home');
        }
        $item_garage = [];
        $total = 0;
        $user = Auth::user();
        if(Session::has('garage')) {
            $garage = Session::get('garage');
            foreach ($garage as $id => $quantity) {
                $item = Item::find($id);
                $item->quantity = $quantity;
                $item->subtotal = $item->price * $quantity;
                $total += $item->subtotal;
                $item_garage[] = $item;
                
            }
        }
        if(Session::get('garage') == []) {
            Session::forget('garage');
        }
        return view('cars.garage', compact('item_garage', 'total'));
    }

    public function removeFromGarage($id) {
        Session::forget("garage.$id");
        return redirect('/cars/garage');
    }

    public function BuyOne($id) {
        $order = new Order;
        $order->user_id = Auth::user()->id;
        $order->total = 0;
        $order->status_id = 3;
        $order->transaction_code = time()."EVC".time();
        $total = 0;
        $order->save();
        foreach (Session::get('garage') as $car => $quantity) {
            if ($car == $id) {
                $order->items()->attach($car, ['quantity'=>$quantity]);
                $item = Item::find($car);
                $total += $item->price * $quantity;
            }
        }

        $order->total = $total;
        $order->save();

        Session::forget("garage.$id");
        Session::flash('success_message', 'Successfully order, waiting for Approval');
        return redirect::back();
    }

    public function showTransaction() {
        $orders = Order::where("orders.user_id", Auth::user()->id)->get();
        $statuses = Status::all();
        $items = Item::all();
        if (Auth::user()->status == 'deactive') {
            return redirect('/home');
        }
        if ($orders->isEmpty()) {
            $orders = [];
            return view('transaction', compact('orders', 'statuses', 'items'));
        }
        return view('/transaction', compact('orders', 'statuses', 'items'));
    }

    public function showAllTransaction() {
        $orders = Order::all();
        $statuses = Status::all();
        if (Auth::user()->role == 'user') {
            return redirect('/home');
        }
        return view('/transactionAll', compact('orders', 'statuses'));

    }

    public function deleteTransact($id) {
        $order = Order::find($id);
        $order->delete();
        return redirect::back();
    }

    public function editTransaction($id, Request $request) {
        $order = Order::find($id);
        $order->status_id = $request->transactionStatus;

        if ($order->status_id == 1) {
            $item = Item::find($request->itemId);

            if ($item->stocks < $request->quantity || $item->stocks <= 0) {
                Session::flash('success_message', 'Not enough stock');
                return redirect::back();
            }
            $item->stocks -= $request->quantity;
            $item->save();
        }
        $order->save();
        Session::flash('success_message', 'Successfully Updated');
        return redirect::back();
    }

    public function deleteItem($id) {
        $item = Item::find($id);
        $item->delete();

        return back()
        ->with('delete_message', "Item has been successfully delete!")
        ->with('undo_url', "/restore_order/$id");
    }

    public function restoreItem($id) {
        $item = Item::onlyTrashed()->where('id', $id)->first();
        $item->restore();
        Session::flash('success_message', 'Successfully restored');

        return back();
    }

    public function checkout($id) {
        $order = Order::find($id);
        $order->name = 4;
        $order->save();

        return redirect::back();
    }

    public function cancelApprove($id, Request $request) {
        $item = Item::find($request->itemId);
        $item->stocks += $request->quantity;
        $item->save();
        $order = Order::find($id);
        $order->delete();
        
        return redirect::back();

    }

    public function showDeletedTransaction() {
        $orders = Order::onlyTrashed()->get();
        if (Auth::user()->status == 'deactive') {
            return redirect('/home');
        }
        return view('deletedtransaction', compact('orders'));
    }

    public function restoreTransaction($id, Request $request) {
        $item = Item::find($request->ItemId);
        if ($item->stocks >= $request->quantity) {
            $order = Order::onlyTrashed()->where('id', $id)->first();
            $order->restore();
            $item->stocks -= $request->quantity;
            $item->save();
            Session::flash('success_message', 'Successfully restored');
            return back();
        } else {
            Session::flash('success_message', 'Not enough stocks');
            return back();
        }



    }

}
