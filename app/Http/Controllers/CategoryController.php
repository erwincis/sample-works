<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Redirect;
use Auth;

class CategoryController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
    
    public function sort($id) {
    	$category = Category::find($id);
 		$items = $category->items;

 		return view('cars.showCars', compact('items'));
    }

    public function addCategory(Request $request) {
        if (Auth::user()->role == 'user') {
            return redirect('/home');
        }

    	$category = new Category;
    	$category->name = $request->name;
    	$category->save();

    	return redirect('/cars/showCars');
    }

    public function addEdit() {
        $categories = Category::all();
        return view('addCategory', compact('categories'));
    }

    public function deletedCategory($id) {
        $categories = Category::find($id);
        $categories->delete();
        return back();
    }
}
