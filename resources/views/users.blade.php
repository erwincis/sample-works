@extends('layouts.app')

@section('content')

<div class="container-fluid users_list">
	<div class="row">
		<div class="col-md-10 mx-auto">
			<h3 class="text-center mt-4">Users List</h3> 
			<table class="table text-center mt-2" border="1">
				<thead>
					<th>ID #</th>
					<th>Name</th>
					<th>Email</th>
					<th>Role</th>
					<th>Status</th>
					<th>Action</th>
				</thead>
				<tbody>
						@foreach($users as $user)
					<tr>
						<td>{{ $user->id }}</td>
						<td>{{ $user->name }}</td>
						<td>{{ $user->email }}</td>
						<td>{{ $user->role }}</td>
						<td>{{ $user->status }}</td>
						<td>
							@if($user->role == 'admin')
							@else
							<form action="/userslist/{{ $user->id }}" method="POST">
								@csrf
								{{ method_field('PATCH') }}
								<button class="btn btn-primary btn-sm" name="status" value="active">Activate</button>
								<button class="btn btn-secondary btn-sm" name="status" value="deactive">Deactivate</button>
							</form>
							<form action="/userslist/delete/{{$user->id}}" method="POST">
								@csrf
								{{ method_field('DELETE') }}
								<button class="mt-2 btn btn-danger btn-sm" name="status" type="submit">Delete</button>
							</form>
							@endif
						</td>
					</tr>
						@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>

@endsection