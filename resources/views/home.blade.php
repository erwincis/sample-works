@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-4">
            <div >
                <img class="profile-pic" src="https://secondchancetinyhomes.org/wp-content/uploads/2016/09/empty-profile.png"> 
            </div>
        </div>
        <div class="col-md-8 mx-auto">
            <div class="bg-profile">
                <table class="table table-hover mt-5">
                    <thead>
                        <h1 class="text-center">Profile</h1>
                    </thead>
                    <tbody>
                        <tr><td>Name: {{ Auth::user()->name }}</td></tr>
                        <tr><td>Email: {{ Auth::user()->email }}</td></tr>
                        <tr><td>Date Created: {{ Auth::user()->created_at }}</td></tr>
                        <tr><td>Date Updated: {{ Auth::user()->updated_at }}</td></tr>
                        <tr><td>Status: {{ Auth::user()->status }}</td></tr>
                        <tr><td><a href="/editProfile" class="btn btn-block btn-primary">Edit Profile</a></td></tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


@endsection
