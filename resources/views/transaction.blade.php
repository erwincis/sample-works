@extends('layouts.app')
@section('content')

@if($orders != [])
<div class="container-fluid transaction-bg">
	<div class="row">
		<div class="col-md-10 mx-auto">
            <div class="alert-danger">
                {{ Session::get('success_message') }}
            </div>
            <div class="transaction-div">
			     <h1 class="text-center">Transaction History</h1>
                <table class="text-center table-responsive" border="1">
                <thead>
                	<th>Transaction Code</th>
                    <th>Item</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Total</th>
                    <th>Status</th>
                </thead>
                <tbody>
                	@foreach($orders as $order)
                    <tr>
                    	<td>{{ $order->transaction_code }}</td>
                        <td>
	                        @foreach($order->items as $item)
	                        <img src="/{{$item->img_url}}" height="100" alt="">
	                    		{{ $item->name }}
	                    	@endforeach
                        </td>
                        <td>
                        	@foreach($order->items as $item)
	                    		&#8369;{{ $item->price }}
	                    	@endforeach
                        </td>
                        <td>
                        	@foreach($order->items as $item)
	                    		{{ $item->pivot->quantity }}
	                    	@endforeach
                        </td>
                        <td>{{ $order->total }}</td>
                        <td>{{ ucfirst($order->status->name) }}
                            @if($order->status->name == 'waiting for approval')
                            
                        	<form method="POST" action="/cancelOrder/{{ $order->id }}">
                        		@csrf
                        		{{ method_field('DELETE') }}
                        		<button class="btn btn-danger">Cancel</button>
                        	</form>
                            @endif
                            @if($order->status->name == 'approve')
                            <form action="/checkout/{{$order->id}}" method="POST">
                                @csrf
                                <button type="submit" class="btn btn-success btn-sm">Checkout</button>
                            </form>
                                <button onclick="deleteOrder({{$order->id}},
                                    @foreach($order->items as $item)
                                        {{ $item->pivot->quantity }}
                                    @endforeach,
                                    @foreach($order->items as $item)
                                        {{ $item->id }}
                                    @endforeach)" type="button" class="btn btn-danger btn-sm mt-2">Cancel</button>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
		</div>
	</div>
</div>
@foreach($orders as $order)
<div class="modal fade" id="delete_order_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">
              Delete
          </h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            <span id="delete_modal_question"></span>
        </div>
        <div class="modal-footer">
          <form method="POST" id="delete_order_form">
                @csrf
                {{ method_field('DELETE') }}
                <input type="hidden" name="quantity" id="deleteQuantity">
                <input type="hidden" name="itemId" id="deleteItemId">
                <button type="submit" class="btn">
                  Confirm Delete
                </button>
            </form>
              <button type="button" class="btn" data-dismiss="modal">Cancel</button>
        </div>
        </div>
    </div>
</div>
@endforeach
@else
<div class="container-fluid transaction-none">
    <div class="row">
        <div class="col-md-8 mx-auto">
            <div class="jumbotron mt-4">
                <h1 class="text-center">No Transaction made yet</h1>
            </div>
        </div>
    </div>
</div>
@endif

<script>
    function deleteOrder(orderID,quantity,itemID) {
        console.log(itemID);
        var question = document.getElementById("delete_modal_question");
        question.innerHTML = 'Do you want to delete this?';
        $('#delete_order_modal').modal('show');
        document.getElementById("delete_order_form").setAttribute("action", "/transaction/cancel/"+orderID);
        document.getElementById("deleteQuantity").setAttribute("value", quantity);
        document.getElementById("deleteItemId").setAttribute("value", itemID);
        console.log("/transaction/cancel/"+orderID);
    }
</script>

@endsection