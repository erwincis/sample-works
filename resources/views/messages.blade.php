@extends('layouts.app')

@section('content')

<div class="container-fluid">
	<div class="row messages_bg">
		<div class="col-md-8 mx-auto">
			<table class="table text-center mt-5" border="1">
				<thead>
					<th>Email</th>
					<th>Comments</th>
				</thead>
				<tbody>
					@foreach($comments as $comment)
					<tr>
						<td>{{ $comment->email }}</td>
						<td>{{ $comment->comment }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>

@endsection