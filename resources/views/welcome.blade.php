@extends('layouts.app')

@section('content')
        <div class="container-fluid container-image">
            <div class="row">
                <div class="col-md-6 title mx-auto text-center">
                    <div class="store-name mt-5">
	                    <div class="d-inline-block" id="e">E</div>
	                    <div class="d-inline-block" id="v">V</div>
	                    <div class="d-inline-block" id="c">C</div>
                	<h1> Car Dealer </h1>
                	</div>
                </div>
            </div>
            <div class="row">
            	<div class="col-md-6 mx-auto">
            		<div id="demo" class="carousel slide" data-ride="carousel">
					  <ul class="carousel-indicators">
					    <li data-target="#demo" data-slide-to="0" class="active"></li>
					    <li data-target="#demo" data-slide-to="1"></li>
					    <li data-target="#demo" data-slide-to="2"></li>
					  </ul>
					  <div class="carousel-inner">
					    <div class="carousel-item active">
					      <img class="carousel-image" src="/images/carousel1.jpeg" alt="Los Angeles">
					    </div>
					    <div class="carousel-item">
					      <img class="carousel-image" src="/images/carousel2.jpeg" alt="Chicago">
					    </div>
					    <div class="carousel-item">
					      <img class="carousel-image" src="/images/carousel3.jpeg" alt="New York">
					    </div>
					  </div>
					  <a class="carousel-control-prev" href="#demo" data-slide="prev">
					    <span class="carousel-control-prev-icon"></span>
					  </a>
					  <a class="carousel-control-next" href="#demo" data-slide="next">
					    <span class="carousel-control-next-icon"></span>
					  </a>

					</div>
            	</div>
            </div>

        </div>
@endsection