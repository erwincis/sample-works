@extends('layouts.app')

@section('content')

<div class="container-fluid">
	<div class="alert-success text-center">
		{{ Session::get('success_message') }}
	</div>
		<div class="row">
			<header class="col-12 col-md-12">
				<h1 class="text-center pt-3">Contact Us</h1>
			</header>
			<div class="col-md-6">
				<form action="/leavemessage" method="POST">
					@csrf
					@if(!Auth::user())
						<label for="email">Email:</label>
						<input required name="email" id="email" class="form-control" placeholder="Enter your Email here" rows="5">
						</input>
					@endif
					<div class="form-group">
						<label for="Message">Message:</label>
						<textarea required name="message" id="message" class="form-control" placeholder="Enter your Message here" rows="5">
						</textarea>
					</div>
					<button class="btn btn-outline-secondary btn-block">Submit</button>
				</form>
				<div class="container mt-4">
					<div class="row">
						<div class="col-md-6">
							<p>Contact No:</p>
							<p class="text-center">+63 927 446 5200</p>
							<hr>
							<p>Email:</p>
							<p class="text-center">erwin.v.cis@gmail.com</p>
						</div>
						<div class="col-md-6">
							<p>Let's Connect:</p>
							<hr>
							<div class="connect-icons">
								<a href="https://www.facebook.com/hlcis54"><span><i class="devicon-facebook-plain colored"></i></span></a>
								<a href="#"><span><i class="devicon-twitter-plain colored"></i></span></a>
								<a href="https://www.gmail.com"><span><i class="fas fa-envelope"></i></span></a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3866.716465610942!2d121.14116601438515!3d14.269750588925417!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397d896dbc4f11b%3A0x81e5bf0b85615e6!2sSouthville+1+Elementary+School!5e0!3m2!1sen!2sph!4v1553846298123!5m2!1sen!2sph" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>	
		</div>
	</div>


@endsection