<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>EVC Store</title>

    <link rel="stylesheet" href="https://cdn.rawgit.com/konpa/devicon/df6431e323547add1b4cf45992913f15286456d3/devicon.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
    
    
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Ramabhadra&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md shadow-sm">
            <div class="container">
                <div class="logo mr-3">
                    <i class="fas fa-car animated wobble infinite"></i>
                </div>
                <a class="navbar-brand" href="{{ url('/') }}">
                    EVC Car Dealer
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="/contactus">Contact Us</a>
                        </li>
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                        @if(Auth::user()->role == "admin")
                            
                        @endif
                        @if(Auth::user()->role == "user")
                            <li>
                                <a class="nav-link" href="/home">Account</a>
                            </li>
                            <li>
                                <a class="nav-link" href="/transaction">Transaction</a>
                            </li>
                            <li>
                                <a class="nav-link" href="/deletedtransaction">Deleted Transaction</a>
                            </li>
                            <li>
                                <a class="nav-link" href="/cars/showCars">Cars</a>
                            </li>
                            <li>
                                <a class="nav-link" href="/cars/garage">Garage</a>
                            </li>
                            
                        @endif
                            <li class="nav-item dropdown">
                                
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
        <main class="main-bg">
            <div class="container-fluid">
                <div class="row ">
                @auth
                    @if(Auth::user()->role == 'admin')
                    <div class="col-md-2 admin_nav">
                        <ul class="navbar-nav text-center p-3">
                            <li>
                                <a class="nav-link" href="/alltransaction">All Transaction</a>
                            </li>
                            <li>
                                <a class="nav-link" href="/addCategory">Add/Delete Category</a>
                            </li>
                            <li>
                                <a class="nav-link" href="/addNewCar">Add Car</a>
                            </li>
                            <li>
                                <a class="nav-link" href="/cars/showCars">Cars</a>
                            </li>
                            <li>
                                <a class="nav-link" href="/users">Users</a>
                            </li>
                            <li>
                                <a class="nav-link" href="/messages">Messages</a>
                            </li>
                        </ul>
                    </div>
                    @endif
                @endauth
                    <div class="col-md-10 mx-auto p-0">
                        @yield('content')
                    </div>
                </div>
            </div>
        </main>
    </div>
    <footer class="footer navbar-dark">
        <p class="text-center p-3 text-white my-0">
            Powered by Gasoline. Copyright &copy; 2019 Erwin Cis
        </p>
    </footer>
</body>
</html>
