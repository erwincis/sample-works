@extends('layouts.app')

@section('content')

@if(Auth::user()->role == 'admin')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 mx-auto">
		<h3 class="text-center mt-4">Add Category</h3>
			<form action="/addCategory" method="POST">
				@csrf
				{{ method_field('POST') }}
				<div class="form-group">
					<label id="name">Name</label>
					<input required class="form-control" type="text" name="name">
				</div>
				<button type="submit" class="btn btn-block btn-success">Add Category</button>
			</form>
			<h3 class="text-center mt-4">Delete Category</h3>
			<div class="form-group">
				<table class="table text-center">
					<thead>
						<th>Name</th>
						<th>Action</th>
					</thead>
					<tbody>
						@foreach($categories as $category)
						<tr>
							<td>{{ $category->name }}</td>
							<td>
								<form action="/deletecategory/{{$category->id}}" method="POST">
									@csrf
									{{method_field('DELETE')}}
									<button value="{{$category->id}}" name="delete" class="btn btn-sm btn-danger">Delete</button>
								</form>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>	
		</div>	
	</div>
</div>
@else
<div class="container">
	<div class="row">
		<div class="col-md-6 mx-auto">
			<div class="jumbotron mt-5">
				<h1 class="text-center">You're not admin go back to home</h1>
				<a href="/home" class="btn btn-block btn-primary">Go back Home</a>
			</div>
		</div>
	</div>
</div>
@endif

@endsection