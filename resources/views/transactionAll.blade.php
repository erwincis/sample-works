@extends('layouts.app')

@section('content')

@if($orders->isEmpty())
<div class="container-fluid">
    <div class="row no-transaction">
        <div class="col-md-6 mx-auto">
            <div class="jumbotron m-5">
                <h1 class="text-center">No transaction</h1>
            </div>
        </div>
    </div>
    
</div>
@else
<div class="container">
	<div class="row">
		<div class="col-md-8 mx-auto">
			<h1 class="text-center mt-4">All Transaction</h1>
            <div class="alert-danger text-center">
                {{ Session::get('success_message') }}
            </div>
            <table class="table text-center" border="1">
                <thead>
                    <th>Transaction Code</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Current Stocks</th>
                    <th>Total</th>
                    <th>Status</th>
                </thead>
                <tbody>
                	@foreach($orders as $order)
                    <tr>
                        <td>
	                        {{$order->transaction_code}}
                        </td>
                        <td>
                        	@foreach($order->items as $item)
	                    		&#8369;{{ $item->price }}
	                    	@endforeach
                        </td>
                        <td>
                        	@foreach($order->items as $item)
	                    		{{ $item->pivot->quantity }}
	                    	@endforeach</td>
                        <td>
                            @foreach($order->items as $item)
                                {{$item->stocks}}
                            @endforeach
                        </td>
                        <td>{{ $order->total }}</td>
                        <td>{{ ucfirst($order->status->name) }}
                            @if($order->status->name == 'waiting for approval')
                        	<form method="POST" action="/alltransaction/{{ $order->id }}">
                        		@csrf
                        		{{ method_field('PATCH') }}
                                <select name="transactionStatus" id="status" class="form-control">
                                    @foreach($statuses as $status)
                                        @if($status->id != 4)
                                        <option value="{{ $status->id }}"> {{ucfirst($status->name)}}</option>
                                        @endif
                                    @endforeach
                                </select>
                                    <input type="hidden" value="{{$item->pivot->quantity}}" name='quantity'>
                                    <input type="hidden" value="{{ $item->id }}" name=itemId>
                                <button type="submit" class="btn btn-secondary mt-1 btn-block">Save</button>
                        	</form>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
		</div>
	</div>
</div>
@endif
@endsection