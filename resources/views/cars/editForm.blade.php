@extends('layouts.app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-8 mx-auto">
		<h3 class="text-center mt-4">Edit Car</h3>
			<form action="/showCar/edit/{{$car->id}}" enctype="multipart/form-data" method="POST">
				@csrf
				{{ method_field('PATCH') }}
				<div class="form-group">
					<label id="name">Name</label>
					<input required class="form-control" type="text" name="name" value="{{ $car->name }}">
				</div>
				<div class="form-group">
					<label id="description">Description</label>
					<input required class="form-control" type="text" name="description" value="{{ $car->description }}">
				</div>
				<div class="form-group">
					<label id="price">Price</label>
					<input required class="form-control" min="1" type="number" name="price">
				</div>
				<div class="form-group">
					<label id="stocks">Stocks</label>
					<input required class="form-control" min="1" type="number" name="stocks">
				</div>
				<div class="form-group">
					<label id="image">Image</label>
					<input class="form-control-file" type="file" name="image" accept="image/*">
				</div>
				<div class="form-group">
					<label id="category">Category</label>
					<select name="category" id="category" class="form-control">
						@foreach(App\Category::all() as $category)
						<option value="{{ $category->id }}">{{ $category->name }}</option>
						@endforeach
					</select>
				</div>
				
				<button type="submit" class="btn btn-block btn-success">Edit</button>
			</form>
		</div>	
	</div>
</div>

@endsection