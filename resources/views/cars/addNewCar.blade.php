@extends('layouts.app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-8 mx-auto">
		<h3 class="text-center mt-4">Add New Car</h3>
			<form action="/addNewCar" enctype="multipart/form-data" method="POST">
				@csrf
				<div class="form-group">
					<label id="name">Name:</label>
					<input class="form-control" required type="text" name="name">
				</div>
				<div class="form-group">
					<label id="description">Description:</label>
					<textarea name="description" id="description" cols="30" rows="5" class="form-control"></textarea>
				</div>
				<div class="form-group">
					<label id="price">Price:</label>
					<input class="form-control" required min="1" type="number" name="price">
				</div>
				<div class="form-group">
					<label id="stocks">Stocks:</label>
					<input class="form-control" required type="number" name="stocks" min="1">
				</div>
				<div class="form-group">
					<label id="image">Image:</label>
					<input class="form-control-file" type="file" name="image" accept="image/*">
				</div>
				<div class="form-group">
					<label id="category">Category:</label>
					<select name="category" id="category" class="form-control">
						@foreach($categories as $category)
							<option value="{{ $category->id }}">{{ $category->name }}</option>
						@endforeach
					</select>
				</div>
				<button type="submit" class="btn btn-block btn-success">Add New Car</button>
			</form>
		</div>	
	</div>
</div>

@endsection