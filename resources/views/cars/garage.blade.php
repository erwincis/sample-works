@extends('layouts.app')

@section('content')
@if(Session::has('garage'))
<div class="container-fluid">
	<div class="alert-success text-center">
        {{ Session::get('success_message') }}
    </div>
	<div class="row">
		<div class="col-md-10 mx-auto">
			@foreach($item_garage as $item)
			<div class="card flex-row flex-wrap p-1 mt-4">
				<img class="mx-auto card-image" src="/{{$item->img_url}}">
				<div class="mt-1 card-body p-0">
					<table class="table text-center">
						<thead>
							<th>Name</th>
							<th>Description</th>
							<th>Price</th>
							<th>Quantity</th>
							<th>Subtotal</th>
						</thead>
						<tbody>
							<tr>
								<td>{{ $item->name }}</td>
								<td>{{ $item->description }}</td>
								<td>&#8369;{{ $item->price }}</td>
								<td>{{ $item->quantity }}</td>
								<td>&#8369;{{ $item->subtotal }}</td>
							</tr>
								<div class="container">
									<div class="row mb-3">
										<div class="col-md-6 col-12">
											<a href="/cars/garage/BuyOne/{{ $item->id }}" class="btn btn-block btn-primary ml-1" >Buy This</a>
										</div>
										<div class="col-md-6 col-12">
											<a href="/cars/garage/removeFromGarage/{{ $item->id }}" class="btn btn-block btn-danger">Remove This</a>
										</div>
									</div>
								</div>
						</tbody>
					</table>
				</div>
			</div>
			@endforeach
		</div>
	</div>	
</div>
@else
	<div class="container-fluid garage-area-empty">
		<div class="row">
			<div class="col-md-6 mx-auto">
				<div class="jumbotron mt-5">
					<h2 class="text-center">No Cars in the garage</h2>
					<a href="/cars/showCars" class="btn btn-block btn-primary">Back to Car List</a>
				</div>
			</div>
		</div>
	</div>
@endif

@endsection