@extends('layouts.app')

@section('content')
<div class="container-fluid">
    
    <div class="row show_background">
        @if(Auth::user()->role == 'user')
        <div class="col-md-2">
            <ul class="mt-5 text-center category_menu">
                <li class="category-list"><a href="/cars/showCars">All</a></li>
                @foreach(\App\Category::all() as $category)
                <li class="category-list"><a href="/cars/category/{{$category->id}}">{{ $category->name }}</a></li>
                @endforeach
            </ul>
        </div>
        @endif
        <div class="col-md-10 mx-auto">
            <div class="alert-success text-center">
                {{ Session::get('success_message') }}
            </div>
            <div class="alert-success text-center">
                {{ Session::get('delete_message')  }}
                @if(Session::get('undo_url') != NULL )
                <a href="{{ Session::get('undo_url') }}">Restore</a>
                @endif
             </div>
            <h1 class="text-center mt-4">Car List</h1>
            <div class="row">
                @foreach($items as $item)
                <div class="col-md-4 ">
                    <div class="flip-card">
                        <div class="flip-card-inner">
                            <div class="flip-card-front">
                                <img src="/{{$item->img_url}}" width="300" height="300">
                            </div>
                            <div class="flip-card-back">
                                <h5 class="card-title">{{ $item->name }}</h5>
                                <p>Price: &#8369;{{ $item->price }}</p>
                                <p>Stocks: {{ $item->stocks }}</p>
                                <form action="/cars/buyCar/{{ $item->id }}" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <input type="number" max="{{$item->stocks}}" name="quantity" min="1" value="1" class="form-control">
                                    </div>
                                    @if(Auth::user()->role == 'user')
                                    <button class="btn btn-block btn-primary btn-sm" {{ $item->stocks <= 0 ? "disabled" : "" }} type="submit">Garage This</button>
                                    @endif
                                </form>
                                @if(Auth::user()->role == 'admin')
                                <a href="/showCars/edit/{{ $item->id }}" class="btn btn-block btn-sm btn-success mt-1">Edit</a>
                                <button type="button" class="btn btn-block" onclick="deleteOrder({{$item->id}})">Delete</button>
                                @endif
                            </div>
                        </div>
                    </div>
                    
                    <div class="modal fade" id="carDetail{{ $item->id }}">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                            <div class="modal-header">
                              <h4 class="modal-title">{{ $item->name }}</h4>
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body">
                                <img class="card-image-show mx-auto" src="/{{$item->img_url}}">
                                <p>
                                  {{$item->description}}
                                </p>  
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="edit{{$item->id}}">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                            <div class="modal-header">
                              <h4 class="modal-title">{{ $item->name }}</h4>
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body">
                                <img class="card-image-show mx-auto" src="/{{$item->img_url}}">
                                <p>{{$item->description}}</p>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="delete_order_modal{{$item->id}}">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                            <div class="modal-header">
                              <h4 class="modal-title">{{ $item->name }}</h4>
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body p-5">
                                <span id="delete_modal_question{{$item->id}}"></span>
                            </div>
                            <div class="modal-footer p-5">
                              <form method="POST" id="delete_order_form{{$item->id}}">
                                  @csrf
                                  {{ method_field('DELETE')}}
                                  <button type="submit" class="btn">
                                      Confirm Delete
                                  </button>
                              </form>
                                  <button type="button" class="btn" data-dismiss="modal">Cancel</button>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>


<script>
    function deleteOrder(orderId) {
        console.log(orderId);
        var question = document.getElementById("delete_modal_question"+orderId);
        question.innerHTML = 'Do you want to delete this?';
        $('#delete_order_modal'+orderId).modal('show');
        document.getElementById("delete_order_form"+orderId).setAttribute('action', "delete_item/"+orderId);

    }
</script>
@endsection
