@extends('layouts.app')

@section('content')

<div class="container-fluid transaction-bg ">
    <div class="row">
        <div class="col-md-10 mx-auto">
        	<div class="alert-success text-center">
                {{ Session::get('success_message') }}
            </div>
            <h1 class="text-center">Deleted Transaction</h1>
            <div class="transaction-div">
                <table class="table text-center" border="1">
                    <th>Transaction Code</th>
                    <th>Item</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Stocks</th>
                    <th>Action</th>
                <tbody>
                	@foreach($orders as $order)
					@if(Auth::user()->id == $order->user_id)
                    <tr>
                        <td>{{ $order->transaction_code }}</td>
                        <td>
                        	@foreach($order->items as $item)
	                        <img src="/{{$item->img_url}}" height="100" alt="">
	                    		{{ $item->name }}
	                    	@endforeach
                        </td>
                        <td>
                        	@foreach($order->items as $item)
	                    		{{ $item->pivot->quantity }}
	                    	@endforeach
                        </td>
                        <td>
                        	@foreach($order->items as $item)
	                    		&#8369;{{ $item->price }}
	                    	@endforeach
                        </td>
                        <td>
                        	@foreach($order->items as $item)
	                    		{{ $item->stocks }}
	                    	@endforeach
                        </td>
                        <td>
                        	<form action="/restore_deleted/transaction/{{$order->id}}" method="POST">
                        		@csrf
                        		{{ method_field('PATCH') }}
                        		<input type="hidden" name="quantity" value="
								@foreach($order->items as $item)
	                    			{{ $item->pivot->quantity }}
	                    		@endforeach
                        		">
                        		<input type="hidden" name="ItemId" value="
								@foreach($order->items as $item)
	                    		 {{ $item->id }}
	                    		@endforeach
                        		">
                        	<button type="submit" class="btn btn-secondary">Restore</button>
                        	</form>
                        </td>
                    </tr>
					@endif
                    @endforeach
                	</tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection