@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-8 mx-auto">
			<div class="bg-editprofile">
		<h3 class="text-center mt-4">Edit Profile</h3>
			<form action="/editProfile/{{$user->id}}" enctype="multipart/form-data" method="POST">
				@csrf
				{{ method_field('PATCH') }}
				<div class="form-group">
					<label id="name">Name</label>
					<input class="form-control" type="text" name="name" value="{{ $user->name }}">
				</div>
				<div class="form-group">
					<label id="description">Email</label>
					<input class="form-control" type="text" name="email" value="{{ $user->email }}">
				</div>
				<div class="form-group">
					<label id="password">Password</label>
					<input class="form-control" type="text" name="password">
				</div>
				<button type="submit" class="btn btn-block btn-success">Edit Profile</button>
			</form>
		</div>
		</div>	
	</div>
</div>

@endsection