<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/addNewCar', 'ItemController@addNew');
Route::post('/addNewCar', 'ItemController@save');
Route::get('/cars/showCars', 'ItemController@showCars');
Route::get('/editProfile', 'HomeController@editProfile');
Route::get('/addCategory', 'CategoryController@addEdit');
Route::get('/contactus', function () {
	return view('contactus');
});
Route::get('/messages', 'CommentController@showMessage');
Route::post('leavemessage', 'CommentController@leaveMessage');
Route::get('/cars/garage', 'ItemController@garage');
Route::get('/users', 'HomeController@showUsers');
Route::post('/addCategory', 'CategoryController@addCategory');
Route::get('/transaction', 'ItemController@showTransaction');
Route::get('/alltransaction', 'ItemController@showAllTransaction')->middleware('isAdmin');
Route::get('/editCar', 'ItemController@editCar')->middleware('isAdmin');
Route::post('/cars/findCar', 'ItemController@findCarToEdit');
Route::get('/deletedtransaction', 'ItemController@showDeletedTransaction');
Route::delete('/deletecategory/{id}', 'CategoryController@deletedCategory');
Route::patch('/alltransaction/{id}', 'ItemController@editTransaction');
Route::delete('/cancelOrder/{id}', 'ItemController@deleteTransact');
Route::get('/cars/garage/removeFromGarage/{id}', 'ItemController@removeFromGarage');
Route::get('/cars/garage/BuyOne/{id}', 'ItemController@BuyOne');
Route::get('/showCars/delete/{id}', 'ItemController@deleteCar');
Route::patch('/showCar/edit/{id}', 'ItemController@editSave');
Route::get('/showCars/edit/{id}', 'ItemController@editCar');
Route::post('/cars/buyCar/{id}', 'ItemController@buyCar');
Route::patch('/editProfile/{id}', 'HomeController@saveProfile');
Route::get('/cars/category/{id}', 'CategoryController@sort');
Route::patch('/userslist/{id}', 'HomeController@editUser');
Route::delete('/userslist/delete/{id}', 'HomeController@deleteUser');
Route::post('/checkout/{id}', 'ItemController@checkout');
Route::delete('/transaction/cancel/{id}', 'ItemController@cancelApprove');

Route::delete('/cars/delete_item/{id}', 'ItemController@deleteItem');
Route::get('/restore_order/{id}', 'ItemController@restoreItem');
Route::patch('/restore_deleted/transaction/{id}', 'ItemController@restoreTransaction');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


